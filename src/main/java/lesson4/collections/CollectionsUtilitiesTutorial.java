package lesson4.collections;

import java.util.*;

public class CollectionsUtilitiesTutorial {

    public static void main(String[] args) {

        int [] arrayOfNumbers = new int [10];

        Arrays.fill(arrayOfNumbers, 10);

        System.out.println(Arrays.toString(arrayOfNumbers));


        System.out.println(
                Collections.lastIndexOfSubList(
                        Arrays.asList(10,100,90,60, 90, -80, 100, 90, 100, 90, 0), Arrays.asList(0)
                )
        );


        List<String> words = new ArrayList<>();
        Collections.addAll(words, "gsldjfkhsdf", "LITS" , "Automation" ,
                "asdbaksjhd", "LITS" , "Automation" ,
                "asdasdlhgkajhshdk", "gakhsjdaksd", "LITS" , "Automation");

        System.out.println(words);

        Set<String> wordsFromList = new HashSet<>(words);

        System.out.println(wordsFromList);
    }
}
