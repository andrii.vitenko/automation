package lesson4.collections.arrays;

import java.util.Arrays;

public class ArrayTutorial1 {

    public static void main(String[] args) {


        int[] myList1 = {14, 98, 4, 5}; // Print all the array elements
        for (double element: myList1) {
            System.out.println(element);
        }

        String[] myList2 = {"Hello","my", "dear", "array"}; // Print all the array elements
        for (int i=0; i< myList2 .length;i++) {
            System.out.println(myList2[i] + " ");

        }

        String [] words = new String [10];

        for (int i = 0; i < words.length; i++) {
            words[i] = "LITS" + i;
        }

//        Arrays.fill(words, "Hello");
//        System.out.println(Arrays.toString(words));

//        for (int i = 0; i < words.length; i++) {
//            System.out.println(words[i]);
//        }

        int [] indexes = new int[2];

        for (int i = 0; i < words.length; i++) {
            if (words[i].endsWith("8")) {
                indexes[0] = i;
            } else if (words[i].endsWith("5")) {
                indexes[1] = i;
            }
        }

        for (int i = 0; i < indexes.length; i++) {
            System.out.println(indexes[i]);
        }



    }
}
