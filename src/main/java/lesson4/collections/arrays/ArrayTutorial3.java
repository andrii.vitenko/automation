package lesson4.collections.arrays;

public class ArrayTutorial3 {

    public static void main(String[] args) {

        String [] words = {"skjdfhskdf", "ASDASD", "sjdfgksldf", "786rtiyugh", "gagiushd90i", "09yhiugjh", "hasljhgajslk2352gdh"};

        printArrayFormatted(filterWordsByWordsWithDigits(words));
    }

    public static String[] filterWordsByWordsWithDigits(String[] words) {
        int wordsWithDigitsCount = getCountOfWordsThatContainDigits(words);

        printArrayFormatted(words);
        System.out.println("Words with digits = " + wordsWithDigitsCount);

        String [] wordsWithDigit = new String [wordsWithDigitsCount];

        int countWordsWithDigits = 0;
        for(int i = 0; i < words.length; i++) { //i = word
            if (containsDigits(words[i])) {
                wordsWithDigit[countWordsWithDigits] = words[i];
                countWordsWithDigits++;
            }
        }
        return wordsWithDigit;
    }

    public static int getCountOfWordsThatContainDigits(String[] words) {
        int counter = 0;
        for(int i = 0; i < words.length; i++) { //i = word
            if (containsDigits(words[i])) {
                counter++;
            }
        }
        return counter;
    }

    public static boolean containsDigits(String word) {
        String [] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (int j = 0; j < word.length(); j++) { // j = word symbol
            for (int k = 0; k < digits.length; k++) { // k = each digit
                if (String.valueOf(word.charAt(j)).equals(digits[k])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void printArrayFormatted(String[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.print(array[i]);
            } else {
                System.out.print(array[i] + ",");
            }
        }
        System.out.println("]");
    }

}
