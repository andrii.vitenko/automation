package lesson4.collections.arrays;

public class ArrayExamples {

    public int computeSum(int[] nums) {
        int sum = 0;
        for (int i=0; i<nums.length; i++) {
            sum = sum + nums[i];
        }
        return sum;
    }

    int findSmallest(int[] values) {
        int minIndex = 0;  // start with 0th element as min
        for (int i=1; i<values.length; i++) {
            if (values[i] < values[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    }
}
