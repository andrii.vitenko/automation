package lesson4.collections.arrays;

public class MethodsTutorial {

    public static void main(String [] args) {
        int [] numbers = {9, 10, 78, -4, 0, 100};
        printArrayFormatted(numbers);
        printArrayFormatted(insertElementToArray(numbers, 98));
    }

    public static int [] insertElementToArray(int [] array, int numberToInsert) {
        int [] newNumbers = new int [array.length + 1];
        for (int i = 0; i < newNumbers.length; i++) {
            if (i < array.length) {
                newNumbers[i] = array[i];
            } else {
                newNumbers[i] = numberToInsert;
            }
        }

        return newNumbers;
    }

    public static void printArrayFormatted(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                System.out.print(array[i]);
            } else {
                System.out.print(array[i] + ",");
            }
        }
        System.out.println("]");
    }

}
