package lesson4.collections.arrays;

public class ArrayTutorial2 {

    public static void main(String[] args) {

        double [] numbers = new double [10];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = 10.1 * i;
        }

        System.out.print("[");
        for (int i = 0; i < numbers.length; i++) {
            if (i == numbers.length - 1) {
                System.out.print(numbers[i]);
            } else {
                System.out.print(numbers[i] + ",");
            }
        }
        System.out.print("]");
    }
}
