package lesson4.collections;

import java.util.HashSet;
import java.util.Set;

public class SetTutorial {

    public static void main(String[] args) {
        // create a hash set
        Set<String> hs = new HashSet<>();
        // add elements to the hash set
        hs.add("BLUE");
        hs.add("GREEN");
        hs.add("RED");
        hs.add("BROWN");
        hs.add("BLUE");
        hs.add("RED");
        hs.add("MAGENTA");
        hs.add("VIOLET");

        System.out.println(hs);

    }
}
