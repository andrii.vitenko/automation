package lesson4.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListTutorial {

    public static void main(String[] args) {

        List<String> a1 = new ArrayList<>();
        //// ArrayList<int> b = new ArrayList<int>();ошибка компиляци
        a1.add("Zara");
        a1.add("Mahnaz");
        a1.add("Ayan");
        a1.add("Aya4");


//        System.out.println("Iterator output");
//        Iterator<String> iterator = a1.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }
//        for(Iterator<String> itr=a1.iterator(); itr.hasNext();){
//            System.out.println(itr.next());
//        }

        System.out.println(" ArrayList Elements");
        System.out.print("\t" + a1);
//
//
//        System.out.println("\n--------------------------");
//
        List<String> l1 = new LinkedList<String>();
        l1.add("Zara");
        l1.add("Mahnaz");
        l1.add("Ayan");
        System.out.println(" LinkedList Elements");
        System.out.print("\t" + l1);

    }
}
