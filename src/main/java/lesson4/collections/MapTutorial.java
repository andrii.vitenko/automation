package lesson4.collections;

import java.util.HashMap;
import java.util.Map;

public class MapTutorial {
    public static void main(String[] args) {

        Map<String, String> m1 = new HashMap<>();

        m1.put("Petro", "100");
        m1.put("Zara", "8");
        m1.put("Mahnaz", "31");
        m1.put("Ayan", "12");
        m1.put("Daisy", "14");
        m1.put("Ayan", "haisudhfpsa98dfhsuidghiu");

        for(Map.Entry<String, String> entryOfMap : m1.entrySet()) {
            String key = entryOfMap.getKey();
            System.out.println("key: " + key);
            String value = entryOfMap.getValue();
            System.out.println("value: " + value);
        }

        System.out.println(" Map Elements");
        System.out.print("\t" + m1);
    }
}
