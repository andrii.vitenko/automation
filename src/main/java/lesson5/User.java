package lesson5;

public class User {
    private String name;
    private int age;
    private boolean male;

    public User(String name) {
        this.name = name;
    }

    public User(String name, int age, boolean male) {
        this.name = name;
        this.age = age;
        this.male = male;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name - " + name;

    }
}
