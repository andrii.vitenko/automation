package lesson5;

public class App {
    public static void main(String[] args) {
        User user = new User("David");
        user.setAge(21);

        System.out.println(user);
        System.out.println(user.getAge());
    }
}
