package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;

/**
 * Created by itschool on 07.03.2018.
 */
public class MainPage {

    private final WebDriver driver;
    CustomService customService;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        customService = new CustomService(driver);
    }

    @FindBy(id = "header-signin-link")
    private WebElement signLink;

    @FindBy(id = "menu-favorites")
    private WebElement heart;




    public void clickSignLink(){
        signLink.click();
    }

    public void waitForSignLinkCikable(){
      customService.waitClickable(signLink);
    }

    public void waitForHeartClickable(){
       customService.waitClickable(heart);
    }

}
