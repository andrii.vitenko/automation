package services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by itschool on 07.03.2018.
 */
public class CustomService {

    private final WebDriver driver;

    public CustomService(WebDriver driver) {
        this.driver = driver;
    }

    public void waitClickable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public void waitUrl(String url){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void switchToLastWindow(){
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public void goToUrl(String url){
        driver.get(url);
    }
}
