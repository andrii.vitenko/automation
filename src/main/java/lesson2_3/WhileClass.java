package lesson2_3;

public class WhileClass {
    public static void main(String[] args){

        int a=10;
        while(a>0){
            System.out.println("a = "+a);
            a--;
        }
        System.out.println();
        System.out.println("a = "+a);
        System.out.println("and");

//        int n=8;
//        while (n>8){
//            System.out.println("in loop");
//        }
//        System.out.println();
//        System.out.println("out of loop");
//        System.out.println("and");



//        do{
//            System.out.println("a = "+a);
        //n--;
//        }while(a>0); //while(--a >0);
//        System.out.println();
//        System.out.println("a = "+a);
//        System.out.println("end");

    }
}
