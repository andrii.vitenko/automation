package lesson2_3;

public class SwitchClass {
    public static void main(String[] args){
        int a = 2;
        if(a==1){
            System.out.println("a= 1");
        }else if(a==2){
            System.out.println("a=2");
        }else if(a==3){
            System.out.println("a=3");
        }

//        switch(a){
//            case 1:
//                System.out.println("a=1");
//                break;
//            case 2:
//                System.out.println("a=2");
//                break;
//            case 3:
//                System.out.println("a=3");
//                break;
//            default:
//                System.out.println("default");
//        }
//        System.out.println("end");


        

//        String str = "Java";
//        switch(str){
//            case "ava":
//                System.out.println("ava");
//                break;
//            case "Java":
//                System.out.println("Java");
//                break;
//            case "C++":
//                System.out.println("C++");
//                break;
//            default:
//                System.out.println("default");
//        }
//        System.out.println("end");
    }

}
